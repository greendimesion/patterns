
package patterns;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class WhatTest {
    
    @Test
    public void whatInSubject(){
        Pattern pattern = new Pattern(new What("Subject"), null, null);
        assertTrue(pattern.isItSubjectWhat());
    }
    @Test
    public void whatInPredicate(){
        Pattern pattern = new Pattern(null, new What("Predicate"), null);
        assertTrue(pattern.isItPredicateWhat());
    }
    @Test
    public void whatInObject(){
        Pattern pattern = new Pattern(null, null, new What("Object"));
        assertTrue(pattern.isItObjectWhat());
    }
}
