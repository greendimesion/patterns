package patterns;

public class Constant extends Operand {
    
    private final long id;

    public Constant(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }
    
}
